/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.listeners;

import com.devandme.devandmebot.commands.CommandMap;
import com.devandme.devandmebot.commands.defaults.admin.MassMessage;
import com.devandme.devandmebot.commands.defaults.adminbot.RoadToV2Cmd;
import com.devandme.devandmebot.commands.defaults.user.ProblemCommand;
import com.devandme.devandmebot.commands.defaults.user.TutorielCommand;
import com.devandme.devandmebot.core.dataregistery.DataRegistery;
import com.devandme.devandmebot.core.dataregistery.GuildDataRegistery;
import com.devandme.devandmebot.core.dataregistery.PlayerDataRegistery;
import com.devandme.devandmebot.gamel.GameLangage;
import com.devandme.devandmebot.utils.RewardsChecker;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.Event;
import net.dv8tion.jda.core.events.channel.text.TextChannelCreateEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.core.events.user.UserOnlineStatusUpdateEvent;
import net.dv8tion.jda.core.hooks.EventListener;
import net.dv8tion.jda.core.managers.GuildController;

import static com.devandme.devandmebot.utils.Functions.*;

public class BotListener implements EventListener {
	
	private final CommandMap commandmap;

	public BotListener(CommandMap commandmap) {
		this.commandmap = commandmap;
	}
	
	@Override
	public void onEvent(Event event) {
		if(event instanceof MessageReceivedEvent) onMessage((MessageReceivedEvent)event);
		else if(event instanceof GuildMemberJoinEvent) onJoin((GuildMemberJoinEvent)event);
		else if(event instanceof TextChannelCreateEvent) onChannelCreate((TextChannelCreateEvent)event);
		else if(event instanceof MessageReactionAddEvent){
			TutorielCommand.onReact((MessageReactionAddEvent)event);
			GameLangage.onReact((MessageReactionAddEvent)event);
			RoadToV2Cmd.onReact((MessageReactionAddEvent)event);
		}
		else if(event instanceof UserOnlineStatusUpdateEvent) statusUpdateEvent((UserOnlineStatusUpdateEvent) event);
	}
	
	private void statusUpdateEvent(UserOnlineStatusUpdateEvent event){
		User u = event.getUser();
		//Si il a le role mute
		if(event.getMember().getRoles().contains(GuildDataRegistery.getInstance().getMutedRole(event.getGuild()))){
			//Si il est bien mute
			if(PlayerDataRegistery.getInstance().getMuteInfo(u.getIdLong()) != null){
				Long expire = Long.parseLong(PlayerDataRegistery.getInstance().getMuteInfo(u.getIdLong()).get(1));
				//Si le mute a une date d'expiration et qu'il est expiré
				if(!expire.equals(0L) && (System.currentTimeMillis() > expire)){
					//Son mute à pris fin :
					new GuildController(event.getGuild()).removeSingleRoleFromMember(event.getMember(), GuildDataRegistery.getInstance().getMutedRole(event.getGuild())).queue();
					PlayerDataRegistery.getInstance().unmute(event.getUser().getIdLong());
				}
			}
		}
	}

	private void onChannelCreate(TextChannelCreateEvent event) {
		TextChannel tc = event.getChannel();
		tc.createPermissionOverride(GuildDataRegistery.getInstance().getMutedRole(event.getGuild())).setDeny(Permission.MESSAGE_WRITE).queue();
	}

	private void onJoin(GuildMemberJoinEvent event) {
		Guild guild = event.getGuild();
		User user = event.getUser();
		
		PlayerDataRegistery.getInstance().createDataIfHasnt(user.getIdLong());
		
		if(PlayerDataRegistery.getInstance().isMuted(user.getIdLong())){
			new GuildController(event.getGuild()).addSingleRoleToMember(event.getMember(), GuildDataRegistery.getInstance().getMutedRole(guild)).queue();
			return;
		}
		
		PlayerDataRegistery.getInstance().setXp(user.getIdLong(), 100);
		TextChannel spam = GuildDataRegistery.getInstance().getSpamChannel(guild);
		
		spam.sendMessage("[Dev&Me] Bienvenue "+user.getAsMention()+"! Pour obtenir toutes les informations du Discord, utilise les commandes **!help** et **!tutoriel** dans le salon "+spam.getAsMention()+".").queue();
		
		sendPrivateMessage(user, "[Dev&Me] Info | Vous avez reçu 100 xp (Raison : Première connexion).");
		sendPrivateMessage(user, "[Dev&Me] Les règles de bases sont les suivantes : bonne ambiance, respect des autres, entre-aide ! Il est également important de noté que le recrutement est interdit sur tout le Discord.");
	}
	
	private void onMessage(MessageReceivedEvent event) {
		if(event.getAuthor().equals(event.getJDA().getSelfUser())) return;
		if(!(event.getChannel() instanceof TextChannel)) return;
		
		TextChannel tc = event.getTextChannel();
		User user = event.getAuthor();
		Member m = event.getMember();
		String message = event.getMessage().getContentDisplay();
		
		//Add message to count
		if(!message.startsWith(commandmap.getTag())){
			new DataRegistery().addMessage();
			//Add message to playercount
			if(message.length() > 8){
				PlayerDataRegistery.getInstance().addMessage(user.getIdLong(), 1);
				new RewardsChecker(user);
			}
		}
		
		//Only ONE MESSAGE IS ALLOW ON THIS CHANNEL !
		if(tc.getName().equalsIgnoreCase("presentation")){
			tc.createPermissionOverride(m).setDeny(Permission.MESSAGE_WRITE).queue();
			return;
		}
		
		if(MassMessage.waiting.containsKey(user.getIdLong())){
			if(message.equalsIgnoreCase("yes")){
				MassMessage.send(event.getGuild(), MassMessage.waiting.get(user.getIdLong()));
				MassMessage.waiting.remove(user.getIdLong());
				return;
			}
			MassMessage.waiting.remove(user.getIdLong());
		}
		
		if((ProblemCommand.pbc.containsKey(user.getIdLong())) && (tc.getName().equalsIgnoreCase(GuildDataRegistery.getInstance().getSpamChannel(event.getGuild()).getName()))){
			ProblemCommand.validMessage(event.getGuild(), user, message, (TextChannel)event.getChannel());
			event.getMessage().delete().queue();
			return;
		}
		
		if(message.startsWith(commandmap.getTag())){
			//Add commande to count
			new DataRegistery().addCommande();
			
			//Bon salon
			if((tc.getName().equalsIgnoreCase(GuildDataRegistery.getInstance().getSpamChannel(event.getGuild()).getName())) || (m.hasPermission(Permission.ADMINISTRATOR))){
				message = message.replaceFirst(commandmap.getTag(), "");
				commandmap.commandUser(event.getTextChannel(), event.getAuthor(), message, event.getMessage());
			}
			//Mauvais salon
			else{
				sendPrivateMessage(user, "[Dev&Me] Merci d'utiliser le salon '"+GuildDataRegistery.getInstance().getSpamChannel(event.getGuild()).getName()+"' pour les commandes !");
				//Supprimer le message
				if((event.getTextChannel() != null) && (event.getGuild().getSelfMember().hasPermission(Permission.MESSAGE_MANAGE))){
					event.getMessage().delete().queue();
				}
			}
		}
	
	}

}
