/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.gamel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Questions {
	
	private Langage l;
	
	public Questions(Langage l){
		this.l = l;
	}
	
	public HashMap<String, List<String>> getQuestions(){
		HashMap<String, List<String>> qer = new HashMap<String, List<String>>();
		switch (l) {
		
		case HTML:
			qer.put("À quoi correspond la balise <p>", Arrays.asList("Déclarer un paragraphe", "Déclarer une variable", "Déclarer le corps d'une page", "À la protection du code"));
			break;

		case JAVA:
			qer.put("Quel est la façon correct de déclarer une variable de type nombre", Arrays.asList("Integer nombre = 0;", "nombre Integer = 0;", "0 = (Integer)nombre;", "nombre = 0;"));
			break;
			
		default: break;
		}
		return qer;
	}

}
