/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.runnable;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import com.devandme.devandmebot.core.dataregistery.GuildDataRegistery;

public class MessageBrodcaster implements Runnable {

    private final List<String> broadcast;

    private final JDA jda;

    private int index;

    private long mylastmsgid;

    public MessageBrodcaster(Collection<String> broadcast, JDA jda) {
        this.jda = jda;
        this.index = 0;
        this.broadcast = new ArrayList<>(broadcast);
    }

    @Override
    public void run() {

        //anti soam
        if (this.broadcast.isEmpty()) {
            return;
        }

        if (this.index >= this.broadcast.size()) {
            this.index = 0;
        }

        for (Guild g : jda.getGuilds()) {
            if (Objects.requireNonNull(GuildDataRegistery.getInstance().getMainChannel(g)).getLatestMessageIdLong() == mylastmsgid) {
                //Si on est le dernier à avoir envoyer un message, on annule (pour éviter le spam !)
                //Cependant on met le last message à 0L pour que la prochaine fois le messgae soit bien envoyer !
                mylastmsgid = 0L;
                continue;
            }

            mylastmsgid = GuildDataRegistery.getInstance().getMainChannel(g).sendMessage(this.broadcast.get(this.index)).complete().getIdLong();
        }

        this.index++;
    }
}