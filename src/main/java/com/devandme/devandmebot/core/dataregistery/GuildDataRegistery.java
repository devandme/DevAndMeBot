/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.dataregistery;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.devandme.devandmebot.core.DevAndMe;
import com.devandme.devandmebot.utils.ConfigElements;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;

public class GuildDataRegistery {
	
	private static final Path PATH = Paths.get("guilds.json");
	private static final Logger LOGGER = LoggerFactory.getLogger(GuildDataRegistery.class);
	private static GuildDataRegistery ourInstance = new GuildDataRegistery();
	
	private final Map<Long, ConfigElements> guildconf = new HashMap<>();
	
	private GuildDataRegistery(){
		this.load();
	}

	public synchronized void load() {
        LOGGER.debug("Chargement des options de guilds...");

        if(Files.exists(PATH)){
            try(BufferedReader reader = Files.newBufferedReader(PATH)){
                this.guildconf.clear();
                for (GuildData guilddata : DevAndMe.GSON.fromJson(reader, GuildData[].class)) {
                    this.guildconf.put(guilddata.guildId, new ConfigElements(guilddata.mutedRoleName, guilddata.mainChannelName, guilddata.staffChannelName, guilddata.spamChannelName, guilddata.rankspower));
                }
                LOGGER.debug("{} guilds ont été chargés.", this.guildconf.size());
            }catch (IOException e) {
                LOGGER.error("Impossible de charger la liste des guilds !", e);
            }
        }else{
            LOGGER.debug("Aucune guilds chargé, le fichier est inexistant.");
        }
    }

    public synchronized void save() {
        LOGGER.debug("Sauvegarde des guilds...");
        try (BufferedWriter writer = Files.newBufferedWriter(PATH, StandardCharsets.UTF_8)) {
            GuildData[] guilddata = this.guildconf.entrySet().stream().map(GuildData::new).toArray(GuildData[]::new);
            DevAndMe.GSON.toJson(guilddata, writer);
        }catch (IOException e) {
            LOGGER.error("Impossible de charger la liste des guilds !", e);
        }

        LOGGER.debug("{} guilds ont été sauvegardés.", this.guildconf.size());
    }
    
    //Get & Set Data

    public boolean hasData(long guildId){
    	return this.guildconf.containsKey(guildId);
    }
    
    public void createDataIfHasnt(Guild guild, String mutedRole, String mainChannelName, String staffChannelName, String spamChannelName){
    	if(!hasData(guild.getIdLong())){
    		if((mutedRole == null) || (guild.getRolesByName(mutedRole, true) == null)){
    			Role r = guild.getController().createRole().setName("Muted").setHoisted(false).setMentionable(false).complete();
    			for(TextChannel tc : guild.getTextChannels()){
    				tc.createPermissionOverride(r).setDeny(Permission.MESSAGE_WRITE).queue();
    			}
    			mutedRole = r.getName();
    		}
    		if((mainChannelName == null) || (guild.getTextChannelsByName(mainChannelName, true) == null)){
    			TextChannel tc = (TextChannel) guild.getController().createTextChannel("main_channel").complete();
    			mainChannelName = tc.getName();
    		}
    		if((staffChannelName == null) || (guild.getTextChannelsByName(staffChannelName, true) == null)){
    			TextChannel tc = (TextChannel) guild.getController().createTextChannel("staff_logs").complete();
    			staffChannelName = tc.getName();
    		}
    		if((spamChannelName == null) || (guild.getTextChannelsByName(spamChannelName, true) == null)){
    			TextChannel tc = (TextChannel) guild.getController().createTextChannel("commands-and-flood").complete();
    			spamChannelName = tc.getName();
    		}
    		guildconf.put(guild.getIdLong(), new ConfigElements(guild.getRolesByName(mutedRole, true).get(0).getIdLong(), guild.getTextChannelsByName(mainChannelName, true).get(0).getIdLong(), guild.getTextChannelsByName(staffChannelName, true).get(0).getIdLong(), guild.getTextChannelsByName(spamChannelName, true).get(0).getIdLong(), null));
    	}
    }
    
	public void deleteData(long idLong) {
		if(hasData(idLong)){
			guildconf.remove(idLong);
		}
	}

    
    public Role getMutedRole(Guild guild){
    	if(!hasData(guild.getIdLong())) createDataIfHasnt(guild, null, null, null, null);
    	
    	Role r = guild.getRoleById(guildconf.get(guild.getIdLong()).getMutedRoleName());
    	return r;
    }
    
    public TextChannel getMainChannel(Guild guild){
    	if(!hasData(guild.getIdLong())) createDataIfHasnt(guild, null, null, null, null);
    	
    	TextChannel tc = (TextChannel) guild.getTextChannelById(guildconf.get(guild.getIdLong()).getMainChannelName());
    	return tc;
    }
    
    public TextChannel getStaffChannel(Guild guild){
    	if(!hasData(guild.getIdLong())) createDataIfHasnt(guild, null, null, null, null);
    	
    	TextChannel tc = (TextChannel) guild.getTextChannelById(guildconf.get(guild.getIdLong()).getStaffChannelName());
    	return tc;
    }
    
    public TextChannel getSpamChannel(Guild guild){
    	if(!hasData(guild.getIdLong())) createDataIfHasnt(guild, null, null, null, null);
    	
    	TextChannel tc = (TextChannel) guild.getTextChannelById(guildconf.get(guild.getIdLong()).getSpamChannelName());
    	return tc;
    }
    
    public Set<Long> getAllGuildsId(){
    	return guildconf.keySet();
    }
    
    public HashMap<String, Integer> getRanksPowerOn(long guildId){
    	return guildconf.get(guildId).getRanksPower();
    }
    
    //End of Data

    public static GuildDataRegistery getInstance() {
        return ourInstance;
    }

    private final class GuildData {

        public long guildId;

        public Long mutedRoleName, mainChannelName, staffChannelName, spamChannelName;
        public HashMap<String, Integer> rankspower;

        public GuildData(Map.Entry<Long, ConfigElements> entry) {
            this.guildId = entry.getKey();
            this.mutedRoleName = entry.getValue().getMutedRoleName();
            this.mainChannelName = entry.getValue().getMainChannelName();
            this.staffChannelName = entry.getValue().getStaffChannelName();
            this.spamChannelName = entry.getValue().getSpamChannelName();
            this.rankspower = entry.getValue().getRanksPower();
        }
    }
    
}
