/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.dataregistery;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.devandme.devandmebot.core.DevAndMe;
import com.devandme.devandmebot.core.data.ComplexData;

public class PlayerDataRegistery {
	
	private static final Path PATH = Paths.get("playerdata.json");
	private static final Logger LOGGER = LoggerFactory.getLogger(PlayerDataRegistery.class);
	private static PlayerDataRegistery ourInstance = new PlayerDataRegistery();
	
	private final Map<Long, ComplexData> playerdata = new HashMap<>();
	
	private PlayerDataRegistery(){
		this.load();
	}
	
	public synchronized void load() {
        LOGGER.debug("Chargement des playerdata...");

        if(Files.exists(PATH)){
            try(BufferedReader reader = Files.newBufferedReader(PATH)){
                this.playerdata.clear();
                for (PlayerData playerdata : DevAndMe.GSON.fromJson(reader, PlayerData[].class)) {
                    this.playerdata.put(playerdata.userId, new ComplexData(playerdata.power, playerdata.xp, playerdata.messages, playerdata.muted, playerdata.medals));
                }
                LOGGER.debug("{} playerdata ont été chargés.", this.playerdata.size());
            }catch (IOException e) {
                LOGGER.error("Impossible de charger la liste des playersdata !", e);
            }
        }else{
            LOGGER.debug("Aucun playerdata chargé, le fichier est inexistant.");
        }
    }

    public synchronized void save() {
        LOGGER.debug("Sauvegarde des playerdata...");
        try (BufferedWriter writer = Files.newBufferedWriter(PATH, StandardCharsets.UTF_8)) {
            PlayerData[] playerdata = this.playerdata.entrySet().stream().map(PlayerData::new).toArray(PlayerData[]::new);
            DevAndMe.GSON.toJson(playerdata, writer);
        }catch (IOException e) {
            LOGGER.error("Impossible de charger la liste des playerdata !", e);
        }

        LOGGER.debug("{} playerdata ont été sauvegardés.", this.playerdata.size());
    }
    
    //Get & Set Data

    public boolean hasData(long userId){
    	if(this.playerdata.containsKey(userId)){
    		return true;
    	}
    	return false;
    }
    
    public void createDataIfHasnt(long userId){
    	if(!hasData(userId)){
    		List<String> medals = new ArrayList<>();
    		medals.add("Médaille du courage (c'est dangereux par ici)");
    		this.playerdata.put(userId, new ComplexData(0, 0, 0, null, medals));
    	}
    }
    
    public Integer getPower(long userId) {
    	createDataIfHasnt(userId);
        return this.playerdata.get(userId).getPower();
    }
    
    public void setPower(long userId, int power){
    	this.playerdata.put(userId, new ComplexData(power, getXp(userId), getMessages(userId), getMuteInfo(userId), getMedals(userId)));
    }
    
    public Integer getXp(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).getXp();
    }
    
    public void addXp(long userId, int xp){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), (getXp(userId)+xp), getMessages(userId), getMuteInfo(userId), getMedals(userId)));
    }
    
    public void setXp(long userId, int xp){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), xp, getMessages(userId), getMuteInfo(userId), getMedals(userId)));
    }
    
    public int getMessages(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).getMessages();
    }
    
    public void addMessage(long userId, int qtt){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), getXp(userId), (getMessages(userId)+qtt), getMuteInfo(userId), getMedals(userId)));
    }
    
    public List<String> getMuteInfo(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).getMuteInfo();
    }
    
    public boolean isMuted(long userId){
    	createDataIfHasnt(userId);
    	List<String> mutedInfo = this.playerdata.get(userId).getMuteInfo();
    	if((mutedInfo != null) && (Long.parseLong(mutedInfo.get(1)) == 0L) || (Long.parseLong(mutedInfo.get(1)) <= System.currentTimeMillis())){
    		return true;
    	}
    	return false;
    }
    
    public void mute(long userId, String raison, long duration, long userIdLongWhoMute){
    	List<String> m = new ArrayList<>();
    	String until = Long.toString(0L);
    	if(duration != 0L){
    		until = Long.toString((System.currentTimeMillis()+duration));
    	}
    	m.add(raison);
    	m.add(until);
    	m.add(Long.toString(userIdLongWhoMute));
    	this.playerdata.put(userId, new ComplexData(getPower(userId), getXp(userId), getMessages(userId), m, getMedals(userId)));
    }
    
    public void unmute(long userId){
    	this.playerdata.put(userId, new ComplexData(getPower(userId), getXp(userId), getMessages(userId), null, getMedals(userId)));
    }

    public void resetData(long userId){
    	if(hasData(userId)){
    		this.playerdata.remove(userId);
    	}
    }
    
    public List<String> getMedals(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).getMedals();
    }
    
    public int getMedalsCount(long userId){
    	createDataIfHasnt(userId);
    	return this.playerdata.get(userId).getMedals().size();
    }
    
    //End of Data

    public static PlayerDataRegistery getInstance() {
        return ourInstance;
    }

    private final class PlayerData {

        public long userId;

        public int power, xp, messages;
        public List<String> muted, medals;

        public PlayerData(Map.Entry<Long, ComplexData> entry) {
            this.userId = entry.getKey();
            this.power = entry.getValue().getPower();
            this.xp = entry.getValue().getXp();
            this.messages = entry.getValue().getMessages();
            this.muted = entry.getValue().getMuteInfo();
            this.medals = entry.getValue().getMedals();
        }
    }

}
