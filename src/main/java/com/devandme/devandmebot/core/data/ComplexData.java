/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core.data;

import java.util.List;

public class ComplexData {
	
	private int power, xp, messages;
	private List<String> muted, medals;
	
	public ComplexData(int power, int xp, int messages, List<String> muted, List<String> medals) {
		this.power = power;
		this.xp = xp;
		this.messages = messages;
		this.muted = muted;
		this.medals = medals;
	}

	public int getPower() {
		return power;
	}

	public int getXp() {
		return xp;
	}
	
	public int getMessages(){
		return messages;
	}
	
	public List<String> getMuteInfo(){
		return muted;
	}
	
	public List<String> getMedals(){
		return medals;
	}

}
