/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Configuration {

    public static final Path PATH = Paths.get("config.json");

    private Set<String> games;
    private Set<String> broadcast;

    private long gameRotationDelay;

    private TimeUnit gameRotationDelayUnit;

    private String discordToken, gitlabtoken;
    
    private int messagesSend, commandesSend;

    public Set<String> getGames() {
        return games;
    }
    
    public Set<String> getMessagesBroadcast(){
    	return broadcast;
    }

    public void setGames(Set<String> games) {
        this.games = games;
    }

    public long getGameRotationDelay() {
        return gameRotationDelay;
    }

    public void setGameRotationDelay(long gameRotationDelay) {
        this.gameRotationDelay = gameRotationDelay;
    }

    public TimeUnit getGameRotationDelayUnit() {
        return gameRotationDelayUnit;
    }

    public void setGameRotationDelayUnit(TimeUnit gameRotationDelayUnit) {
        this.gameRotationDelayUnit = gameRotationDelayUnit;
    }

    public String getDiscordToken() {
        return discordToken;
    }
    
    public String getGitLabToken(){
    	return gitlabtoken;
    }

    public void setDiscordToken(String discordToken) {
        this.discordToken = discordToken;
    }
    
    public int getMessagesSend(){
    	return messagesSend;
    }
    
    public void setMessagesSend(int messagesSend){
    	this.messagesSend = messagesSend;
    }
    
    public int getCommandesSend(){
    	return commandesSend;
    }
    
    public void setCommandesSend(int commandesSend) {
		this.commandesSend = commandesSend;
	}
    
    public void setGitLabToken(String gitlabtoken){
    	this.gitlabtoken = gitlabtoken;
    }

    public static Configuration load() throws IOException {

        if (Files.notExists(PATH)) {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                    Configuration.class.getResourceAsStream("/com/devandme/devandmebot/config.json")
            ))) {
                Files.write(PATH, reader.lines().collect(Collectors.toList()));
            }
        }

        try (BufferedReader reader = Files.newBufferedReader(PATH, StandardCharsets.UTF_8)) {
            return DevAndMe.GSON.fromJson(reader, Configuration.class);
        }
    }

}