/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.adminbot;

import java.util.HashMap;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.message.react.MessageReactionAddEvent;

public class RoadToV2Cmd {

	//TODO: Uniquement pour le début du la v2.

	private static HashMap<Long, Integer> tutorielpage = new HashMap<>();
	
	protected final static int min = 0;
	protected final static int max = 6;
	
	@Command(name="roadtov2", type=ExecutorType.S_USER)
	private void power(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		Message mes = channel.sendMessage(loadPage(min)).complete();
		tutorielpage.put(mes.getIdLong(), min);
		/*
		 * React : 
		 * http://unicode.org/emoji/charts/full-emoji-list.html
		 * https://apps.timwhitlock.info/emoji/tables/unicode
		 * (Prendre les natifs)
		 */
		mes.addReaction("⏪").queue();
		mes.addReaction("⏩").queue();
	}

	public static void onReact(MessageReactionAddEvent event) {
		if((tutorielpage.containsKey(event.getMessageIdLong())) && (!event.getUser().isBot())){
			String reactEmot = event.getReactionEmote().getName().toString();
			
			event.getTextChannel().clearReactionsById(event.getMessageIdLong()).complete();
			event.getTextChannel().addReactionById(event.getMessageIdLong(), "⏪").queue();
			event.getTextChannel().addReactionById(event.getMessageIdLong(), "⏩").queue();
			
			if((reactEmot.equalsIgnoreCase("⏪")) || (reactEmot.equalsIgnoreCase("⏩"))){
				int pageNumber = tutorielpage.get(event.getMessageIdLong());
				event.getTextChannel().editMessageById(event.getMessageId(), (reactEmot.equalsIgnoreCase("⏪") ? loadPage(((pageNumber-1) < min) ? min : pageNumber-1) : loadPage(((pageNumber+1) > max) ? max : pageNumber+1))).queue();
				tutorielpage.remove(event.getMessageIdLong());
				tutorielpage.put(event.getMessageIdLong(), (reactEmot.equalsIgnoreCase("⏪") ? pageNumber-1 : pageNumber+1));
			}
		}
	}
	
	private static String loadPage(int pageId){	
		String content = "_Erreur lors du chargement !_";
		switch (pageId) {
		case 0: 
			content = "Dev & Me est passé en **version 2**. Nous avons opéré beaucoup de changements au Discord pour le rendre encore meilleur !";
			break;
		case 1:
			content = "**Concernant les salons:**\n\n    - La plupart des salons ont été supprimer dans l'objectif de rendre le Discord plus clair et lisible. Il ne reste plus que des salons aux noms explicites. Les salons vocaux ont quand à eux été placé tout en haut pour être plus visible.\n    - Les salons spéciaux comme #presentation continuent de suivre les mêmes règles, à savoir, un seul message autorisé. Le salon #bot-et-spam est quant à lui toujours réserver aux commandes de bot ; salon que nous vous conseillons fortement de mettre en muet _(Clique droit >> Rendre Muet #bot-et-spam)_.\n    - Vu que vous nous l'avez massivement demandé, la publicité, le recrutement et la présentation de vos projets sont maintenant autorisés mais uniquement dans le salon #pub-et-recrutement. Cependant, gardé en tête que les développeurs présents sur ce Discord ne sont pas là pour être recruté, nous vous demandons donc de ne pas venir dans cette optique précise. Nous nous réservons le droit de vous sanctionner si vous rejoignez le Discord uniquement dans un but de recrutement.";
			break;
		case 2:
			content = "**Concernant le bot:**\n\nNous avons complètement revu le bot pour le rendre plus performant, plus utiles et nous lui avons ajouté plein de fonctionnalités cool. Les commandes doivent être faites dans le salon #bot-et-spam pour ne pas déranger les autres membres. Vous pouvez avoir un apercu de toutes les commandes avec **!help** et un petit tutoriel explicatif avec **!tutoriel**. Vous pouvez toujours participer au développement du Bot sur https://gitlab.com/devandme/DevAndMeBot.";
			break;
		case 3:
			content = "**Concernant les rôles:**\n\n    - Les grades ont été repenser. Il y en avait ici aussi trop, nous avons donc décidé d'en supprimer la majeure partie et de les ranger dans trois catégories: **Staff** (admins, modérateurs, supports et bots), **Ami** (partenaires, amis et ancien staff) et **Développeur** (regroupe tous les rôles relatifs aux langages de programmation). Une liste complète et mise à jour automatiquement des grades de la catégorie **Développeur** est disponible avec la commande **!role**.\n    - Les critères pour obtenir les grades correspondant aux langages ont également changé. Il faudra prouver que vous avez de l'expérience avec un langage et que vous êtes apte à fournir une aide satisfaisante pour obtenir le grade du langage correspondant. Ces grades n'apportent aucun avantage autre que visuel (ils permettent simplement de reconnaitre les personnes réellement qualifiées); vous êtes toujours invité à aider les autres même sans grade, vous finirez forcément par en obtenir un !\n    - Les personnes ayant fait une présentation (#presentation) obtiendront le grade **Développeur** (et seront donc classées dans cette catégorie) sans vérification de compétences. Ce grade permet simplement de vous démarquer dans la liste des membres et de montrer votre soutien au projet. Et puis, c'est beaucoup plus sympatique de se connaitre un peu !";
			break;
		case 4:
			content = "**Concernant le support:**\n\nVous l'aurez sûrement remarqué, les salons relatifs aux langages et au support ont disparu. Ce n'est pas que nous avons décidé d'abandonner cet aspect, pas du tout, nous l'avons même rendu beaucoup plus simple pour vous apporter des réponses personnelles, plus adapté et plus rapide. Dans cette optique, vous serez invité à créer votre propre salon pour chacun de vos problèmes. Pour ce faire, il suffit d'utiliser la commande **!problem create**. Vous devrez ensuite préciser une liste de langages ainsi qu'un titre. Une fois fait, votre salon textuel sera créé et les autres membres pourront vous aider. Voici un exemple de commande pour ouvrir un nouveau problème: **!problem create Html,Css,Php À quoi correspondent ces languages ?**. Une fois votre problème résolu, vous êtes invité à le fermé en rentrant la commande: **!problem solved (id)**. Toutes les commandes relatives aux problèmes sont visibles via **!problem**.";
			break;
		case 5:
			content = "**Les sanctions:**\n\nNotre équipe se réserve le droit, à tout moment, de vous appliquer une sanction s'il juge que vous n'êtes pas conforme aux règles de bien-vivre ensemble ou spécifiques au Discord. Il existe 5 sanctions possibles et combinables sur le Discord :\n    - La suppression d'un ou plusieurs de vos messages: Les membres de l'équipe peuvent supprimer vos/votre messages s'ils sont jugé comme non conformes aux règles. Cette sanction est souvent combinée avec un avertissement.\n    - L'avertissement: Le membre de l'équipe, généralement le modérateur, vous avertis en privé que votre comportement n'est pas adapté ou que vous faites quelque chose de non autorisé. Vous êtes donc invité à cesser cette chose pour ne pas avoir une plus grosse sanction.\n    - La réduction au silence (temporaire ou définitif): Vous n'avez plus la permission de parler dans aucun salon textuel. Vous conservez la parole dans les salons vocaux à moins que l'on vous l'enlève également. Cette sanction est souvent mises en oeuvre lorsque vous avez trop enfrin les règles.\n    - L'exclusion: Vous êtes exclus du serveur mais pouvez y revenir tout de suite dans la foulée. Vous perdez cependant vos grades et votre ancienneté. Cette sanction est mise en oeuvre pour vous faire réfléchir sur votre comportement\n    - Le bannissement (temporaire ou définitif): Vous êtes exclus du Discord et ne pouvez plus y revenir du tout tant que votre bannissement est actif. S'il prend fin, vous pourrez revenir sur le Discord mais vous aurez perdu toutes vos données (grade, ancienneté, etc...).";
			break;
		case 6:
			content = "**Autres informations:**\n\n    - Le Discord intègre un système d'expérience. Vous gagner de l'expérience en envoyant des messages ou en aidant les autres membres !\n    - Le Discord intègre également un système de médailles. Ces dernières sont données à la suite de bons comportements ou après avoir réalisé une certaine action (nombre de messages, etc...). Les administrateurs peuvent aussi distribuer des médailles personnalisées, rien que pour vous.\n\n\nUn énorme merci à toute l'équipe pour son aide et son travail sur la version 2 et merci à vous d'être aussi nombreux sur ce Discord. Nous sommes toujours ouvert aux retours, suggestions et améliorations que nous pourrions apporter,\n\nMathéo - DiscowZombie, pour l'équipe de Dev & Me.";
			break;
		default:
			break;
		}
		return content;
	}

}
