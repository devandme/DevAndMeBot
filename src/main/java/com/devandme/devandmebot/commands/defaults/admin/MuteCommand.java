/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.admin;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.core.dataregistery.GuildDataRegistery;
import com.devandme.devandmebot.core.dataregistery.PlayerDataRegistery;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.managers.GuildController;

import static com.devandme.devandmebot.utils.Functions.*;

import java.util.concurrent.TimeUnit;

public class MuteCommand {
	
	@Command(name="mute", type=ExecutorType.ALL_USER, description="Permet de rendre silencieux", power=70)
	private void mute(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		if(args.length <= 2 || message.getMentionedUsers().size() == 0 || (args[1] == null)){
			channel.sendMessage("mute <@user> <duration:P|Y|M|W|d|h|m> <raison>").queue();
			channel.sendMessage("P: à vie; Y: année; M: mois; W: semaine; d: jour; h: heure; m: minute.").queue();
			channel.sendMessage("Exemple: !mute @user 1W Raison...").queue();
			return;
		}
		
		String complexduration = args[1];
		Long duration = 0L;
		if(complexduration.contains("m")){
			Long i = Long.parseLong((complexduration.replaceAll("m", "")));
			duration = (i * 60000L);
		}else if(complexduration.contains("h")){
			Long i = Long.parseLong((complexduration.replaceAll("h", "")));
			duration = (i * 3600000L);
		}else if(complexduration.contains("d")){
			Long i = Long.parseLong((complexduration.replaceAll("d", "")));
			duration = (i * 86400000L);
		}else if(complexduration.contains("W")){
			Long i = Long.parseLong((complexduration.replaceAll("W", "")));
			duration = (i * 604800000L);
		}else if(complexduration.contains("M")){
			Long i = Long.parseLong((complexduration.replaceAll("M", "")));
			duration = (i * 2629746000L);
		}else if(complexduration.contains("Y")){
			Long i = Long.parseLong((complexduration.replaceAll("Y", "")));
			duration = (i * 31556952000L);
		}

		User toMute = message.getMentionedUsers().get(0);
		
		StringBuilder sb = new StringBuilder();
        for (int j = 2; j < args.length; j++) {
          sb.append(args[j]).append(" ");
        }
        String raison = sb.toString().trim();
		
        message.delete().queue();
        
        //OL => À vie
        PlayerDataRegistery.getInstance().mute(toMute.getIdLong(), raison, duration, user.getIdLong());
		//Rendre muet l'utilisateur
		new GuildController(guild).addRolesToMember(new GuildController(guild).getGuild().getMember(toMute), GuildDataRegistery.getInstance().getMutedRole(guild)).queue();
		sendPrivateMessage(toMute, "[Dev&Me] Vous avez été réduit au silence "+(duration == 0L ? "à vie" : "durant "+displayDuration(duration))+": **"+raison+"**.");
        
		GuildDataRegistery.getInstance().getStaffChannel(guild).sendMessage("**"+user.getAsMention()+"** a réduit "+(duration == 0L ? "à vie" : "durant "+displayDuration(duration))+" au silence **"+toMute.getAsMention().toString()+"** pour **"+raison+"**.").queue();
	}
	
	private String displayDuration(Long millis){
        StringBuilder sb = new StringBuilder(64);
	
		if(millis > 0){
	        long days = TimeUnit.MILLISECONDS.toDays(millis);
	        millis -= TimeUnit.DAYS.toMillis(days);
	        long hours = TimeUnit.MILLISECONDS.toHours(millis);
	        millis -= TimeUnit.HOURS.toMillis(hours);
	        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
	        millis -= TimeUnit.MINUTES.toMillis(minutes);
	        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);
	
	        if(days > 0) sb.append(days + " jour"+(days > 1 ? "s" : ""));
	        if(hours > 0) sb.append(" " + hours + " heure"+(hours > 1 ? "s" : ""));
	        if(minutes > 0) sb.append(" " + minutes + " minute"+(minutes > 1 ? "s" : ""));
	        if(seconds > 0) sb.append(" " + seconds + " seconde"+(seconds > 1 ? "s" : ""));
		}
		return sb.toString();
	}
	
	@Command(name="unmute", type=ExecutorType.ALL_USER, description="Permet de redonner la parole", power=70)
	private void unmute(Guild guild, User user, MessageChannel channel, Message message, String[] args){
		if(message.getMentionedUsers().size() == 0){
			channel.sendMessage("unmute <@user>").queue();
			return;
		}
		
		User toUnmute = message.getMentionedUsers().get(0);
		
		if(!PlayerDataRegistery.getInstance().isMuted(toUnmute.getIdLong())){
			//Isn't muted
			channel.sendMessage("Cet utilisateur n'est pas silencieux !").queue();
			return;
		}
		
		message.delete().queue();
		
		PlayerDataRegistery.getInstance().unmute(toUnmute.getIdLong());
		new GuildController(guild).removeRolesFromMember(new GuildController(guild).getGuild().getMember(toUnmute), GuildDataRegistery.getInstance().getMutedRole(guild)).queue();
		sendPrivateMessage(toUnmute, "[Dev&Me] Vous avez retrouver la parole !");
		
		GuildDataRegistery.getInstance().getStaffChannel(guild).sendMessage("**"+user.getAsMention()+"** a rédonné la parole à **"+toUnmute.getAsMention().toString()+"**.").queue();
	}

}
