/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.user;

import com.devandme.devandmebot.commands.Command;

import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.User;

public class QuizCommand {
	
	@Command(name="quiz", description="Permet de se divertir avec divers quiz !")
	private void langage(User user, MessageChannel channel, Message message, String[] args){
		channel.sendMessage("*Cette fonctionalité n'est pas encore prête ! Patentiez un peu...*").queue();
		
		/*if((args.length == 1) && (args[0] != null) && (Utils.checkLangageSyntax(args[0]) != null)){
			Langage lcible = Utils.checkLangageSyntax(args[0]);
			new GameLangage().start(user, Langage.JAVA, (TextChannel)channel);
			return;
		}
		
		channel.sendMessage("quiz <langage>").queue();
		channel.sendMessage("Quiz disponibles : "+Utils.displayLangageList(Langage.values())+".").queue();*/
	}

}
