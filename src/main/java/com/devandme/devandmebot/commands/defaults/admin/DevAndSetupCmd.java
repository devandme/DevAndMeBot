/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands.defaults.admin;

import java.util.List;

import com.devandme.devandmebot.commands.Command;
import com.devandme.devandmebot.core.dataregistery.GuildDataRegistery;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.TextChannel;
import net.dv8tion.jda.core.entities.User;

public class DevAndSetupCmd {
	
	@Command(name="setup", power=100)
	private void setup(Guild guild, MessageChannel channel, Message message, String[] args){
		List<Role> mentionnedr = message.getMentionedRoles();
		List<TextChannel> mentionnedc = message.getMentionedChannels();
		
		boolean meetRequire = (
				mentionnedr.size() == 1 && 
				mentionnedr.get(0) != null && 
				mentionnedc.size() == 3 && 
				mentionnedc.get(0) != null && 
				mentionnedc.get(1) != null &&
				mentionnedc.get(2) != null
		);
		
		if(!meetRequire){
			channel.sendMessage("Votre message ne remplis pas toutes les conditons: Un rôle (muted) et trois salons textuels (main, staff, commands-spam) sont nécessaires").queue();
			return;
		}
		
		Role muted = mentionnedr.get(0);
		TextChannel main = mentionnedc.get(0), staff = mentionnedc.get(1), cs = mentionnedc.get(2);
		
		if(GuildDataRegistery.getInstance().hasData(guild.getIdLong())){
			if(!args[0].equalsIgnoreCase("--confirm")){
				channel.sendMessage("Des données existent déjà pour ce serveur ! Pour les supprimer, merci de rajouter **--confirm** comme premier argument.").queue();
				return;
			}
			
			channel.sendMessage("Suppresion des données pour "+guild.getIdLong()+"...").queue();
			GuildDataRegistery.getInstance().deleteData(guild.getIdLong());
		}
		
		channel.sendMessage("Tentatives de création des données...").queue();
		GuildDataRegistery.getInstance().createDataIfHasnt(guild, muted.getName(), main.getName(), staff.getName(), cs.getName());
		
		if(GuildDataRegistery.getInstance().hasData(guild.getIdLong())){
			channel.sendMessage("Les données ont été créé avec succès.").queue();
		}else{
			channel.sendMessage("La création des données a échoué !").queue();
			return;
		}
		
		displayInfo(guild, channel);
	}
	
	@Command(name="debug", power=100)
	private void debug(Guild guild, Message message, MessageChannel channel){
		if(message.getMentionedUsers() != null && message.getMentionedUsers().size() > 0){
			for(User u : message.getMentionedUsers()){
				channel.sendMessage(u.getAsMention()+" - "+u.getId()+"/"+u.getIdLong()).queue();
			}
		}
		if(message.getMentionedRoles() != null && message.getMentionedRoles().size() > 0){
			for(Role r : message.getMentionedRoles()){
				channel.sendMessage(r.getAsMention()+" - "+r.getId()+"/"+r.getIdLong()).queue();
			}
		}
		if(message.getMentionedChannels() != null && message.getMentionedChannels().size() > 0){
			for(TextChannel c : message.getMentionedChannels()){
				channel.sendMessage(c.getAsMention()+" - "+c.getId()+"/"+c.getIdLong()).queue();
			}
		}
	}
	
	@Command(name="debugad", power=100)
	private void debugad(Guild guild, MessageChannel channel){
		displayInfo(guild, channel);
	}
	
	private void displayInfo(Guild guild, MessageChannel channel){
		Role muted = GuildDataRegistery.getInstance().getMutedRole(guild);
		TextChannel staff = GuildDataRegistery.getInstance().getStaffChannel(guild);
		TextChannel spam = GuildDataRegistery.getInstance().getSpamChannel(guild);
		
		channel.sendMessage("Guild Id: "+guild.getIdLong()+"\n\nMutedRole: "+muted.getAsMention()+"\nStaffChannel: "+staff.getAsMention()+"\nSpamChannel: "+spam.getAsMention()).queue();
	}

}
