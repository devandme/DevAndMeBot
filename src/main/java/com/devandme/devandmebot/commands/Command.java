/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(value = ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Command {

    String name();

    String description() default "Aucune description.";

    ExecutorType type() default ExecutorType.ALL;

    //Important : Un power de "-1" permet de masquer la commande de l'affichage dans le !help.
    int power() default 0;

    enum ExecutorType {
        ALL, //Tout le monde
        ALL_USER, //Tous les utilisateurs (tout les personnes sur Discord)
        CONSOLE, //Seulement depuis la console
        CONSOLE_OR_S_USER, //La console ou les super admisitrateurs
        S_USER //Seulements les administrateurs du bot (appeler "super admins")
    }
}