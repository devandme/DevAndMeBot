/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands;

import com.devandme.devandmebot.commands.Command.ExecutorType;
import com.devandme.devandmebot.commands.defaults.BasicCommand;
import com.devandme.devandmebot.commands.defaults.admin.DevAndSetupCmd;
import com.devandme.devandmebot.commands.defaults.admin.MassMessage;
import com.devandme.devandmebot.commands.defaults.admin.MuteCommand;
import com.devandme.devandmebot.commands.defaults.admin.PowerCommand;
import com.devandme.devandmebot.commands.defaults.adminbot.RoadToV2Cmd;
import com.devandme.devandmebot.commands.defaults.adminbot.SuperAdminCmd;
import com.devandme.devandmebot.commands.defaults.user.MeCommand;
import com.devandme.devandmebot.commands.defaults.user.MedalsCommand;
import com.devandme.devandmebot.commands.defaults.user.ProblemCommand;
import com.devandme.devandmebot.commands.defaults.user.QuizCommand;
import com.devandme.devandmebot.commands.defaults.user.RolesCommand;
import com.devandme.devandmebot.commands.defaults.user.StatsCommand;
import com.devandme.devandmebot.commands.defaults.user.TutorielCommand;
import com.devandme.devandmebot.core.Configuration;
import com.devandme.devandmebot.core.DevAndMe;
import com.devandme.devandmebot.core.dataregistery.GuildDataRegistery;
import com.devandme.devandmebot.core.dataregistery.PlayerDataRegistery;
import com.devandme.devandmebot.core.dataregistery.SupportRegistery;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class CommandMap {

	private static final Logger LOGGER = LoggerFactory.getLogger(CommandMap.class);

	private final Map<String, SimpleCommand> commands = new HashMap<>();
	private final JDA jda;

	//Power des grades. Premier (Long): Id de guild. Deuxième (String): Id du grade. Troisième (Integer): power entre 0 et 100.
	private final Map<Long, Map<String, Integer>> rankspower = new HashMap<>();
	
	//Les supers admins
	private final ArrayList<String> superadmins = new ArrayList<>(Arrays.asList("208986158665957376"));
	
    public CommandMap(DevAndMe devAndMe, Configuration configuration){
		this.jda = devAndMe.getJda();

		this.registerCommands(
				new BasicCommand(devAndMe, this, configuration.getGitLabToken()), 
				new PowerCommand(this), 
				new StatsCommand(), 
				new QuizCommand(), 
				new MeCommand(this), 
				new MuteCommand(),
				new ProblemCommand(this),
				new RolesCommand(),
				new MedalsCommand(),
				new TutorielCommand(),
				new SuperAdminCmd(),
				new MassMessage(),
				new DevAndSetupCmd(),
				new RoadToV2Cmd()
				);
		
		GuildDataRegistery.getInstance().load();
		PlayerDataRegistery.getInstance().load();
		SupportRegistery.getInstance().load();
		
		for(Long guildLong : GuildDataRegistery.getInstance().getAllGuildsId()){
			rankspower.put(guildLong, GuildDataRegistery.getInstance().getRanksPowerOn(guildLong));
		}
	}
    
    public int getTotalUserPower(Guild guild, User user){
    	return Math.max(getPersonalUserPower(guild, user), getRankUserPower(guild, user));
    }
    
    public int getRankUserPower(Guild guild, User user){
    	if(guild.getMember(user).hasPermission(Permission.ADMINISTRATOR)) return 100;
    	
    	int rankpowerbonus = 0;
    	for(Role r : guild.getMember(user).getRoles()){
    		if(rankspower.get(guild.getIdLong()).containsKey(r.getId())){
    			rankpowerbonus = rankspower.get(guild.getIdLong()).get(r.getId());
    		}
    	}
    	
    	return rankpowerbonus;
    }

	public int getPersonalUserPower(Guild guild, User user){
		return PlayerDataRegistery.getInstance().getPower(user.getIdLong()) != null ? PlayerDataRegistery.getInstance().getPower(user.getIdLong()) : 0;
	}

	public void setPersonalUserPower(User user, int power){
		if(power == 0) removePersonnalUserPower(user);
		else PlayerDataRegistery.getInstance().setPower(user.getIdLong(), power);
	}

	public void removePersonnalUserPower(User user){
		PlayerDataRegistery.getInstance().setPower(user.getIdLong(), 0);
	}
	
	public boolean isSuperAdmin(User user){
		return (superadmins.contains(user.getId().toString()));
	}

    public String getTag(){
        return "!";
    }

    public Collection<SimpleCommand> getCommands(){
        return commands.values();
    }

    public void registerCommands(Object...objects){
        for(Object object : objects){
        	registerCommand(object);
        }
    }

    public void registerCommand(Object object){
        for(Method method : object.getClass().getDeclaredMethods()){
            if(method.isAnnotationPresent(Command.class)){
                Command command = method.getAnnotation(Command.class);
                method.setAccessible(true);
                SimpleCommand simpleCommand = new SimpleCommand(command.name(), command.description(), command.power(), command.type(), object, method);
                commands.put(command.name(), simpleCommand);
            }
        }
    }

    public void commandConsole(String command){
        Object[] object = getCommand(command);
        if((object[0] == null) || (((SimpleCommand)object[0]).getType() != ExecutorType.CONSOLE) && (((SimpleCommand)object[0]).getType() != ExecutorType.ALL) && (((SimpleCommand)object[0]).getType() != ExecutorType.CONSOLE_OR_S_USER)){
        	LOGGER.error("Cette commande est inconnue ou elle ne peut être exécutée par la console.");
            return;
        }
        try{
            execute(((SimpleCommand)object[0]), command, (String[])object[1], null);
        }catch(Exception exception){
        	LOGGER.error("Impossible d'exécuter la commande: La méthode "+((SimpleCommand)object[0]).getMethod().getName()+" n'est pas correctement initialisé.");
            exception.printStackTrace();
        }
    }

    public boolean commandUser(TextChannel textchannel, User user, String command, Message message){
        Object[] object = getCommand(command);
        if((object[0] == null) || (((SimpleCommand)object[0]).getType() == ExecutorType.CONSOLE)) return false;
        if(message.getGuild() == null) return false;
        //Vérifier qu'il ne faut pas être super admin pour éxecuter cette commande
        boolean needSAdminPerms = (((((SimpleCommand) object[0]).getType() == ExecutorType.S_USER)) || ((((SimpleCommand) object[0]).getType() == ExecutorType.CONSOLE_OR_S_USER)));
        if(needSAdminPerms && !isSuperAdmin(user)){
        	textchannel.sendMessage("_Permissions insuffisantes: Vous devez être super-administrateur pour faire ceci._").queue();
        	return false;
        }
        if(((SimpleCommand)object[0]).getPower() > getTotalUserPower(message.getGuild(), message.getAuthor())){
        	textchannel.sendMessage("_Permissions insuffisantes: Vous n'avez pas assez de power pour faire ceci._").queue();
        	return false;
        }
        
        try{
            execute(((SimpleCommand)object[0]), command,(String[])object[1], message);
        }catch(Exception exception){
        	LOGGER.error("Impossible d'exécuter la commande: La methode "+((SimpleCommand)object[0]).getMethod().getName()+" n'est pas correctement initialisé.");
            exception.printStackTrace();
        }
        return true;
    }

    private Object[] getCommand(String command){
        String[] commandSplit = command.split(" ");
        String[] args = new String[commandSplit.length-1];
        for(int i = 1; i < commandSplit.length; i++){
        	args[i-1] = commandSplit[i];
        }
        SimpleCommand simpleCommand = commands.get(commandSplit[0]);
        return new Object[]{simpleCommand, args};
    }

    private void execute(SimpleCommand simpleCommand, String command, String[] args, Message message) throws Exception{
        Parameter[] parameters = simpleCommand.getMethod().getParameters();
        Object[] objects = new Object[parameters.length];

        for(int i = 0; i < parameters.length; i++){
        	if(parameters[i].getType() == String[].class) objects[i] = args;
            else if(parameters[i].getType() == User.class) objects[i] = message == null ? null : message.getAuthor();
            else if(parameters[i].getType() == TextChannel.class) objects[i] = message == null ? null : message.getTextChannel();
            else if(parameters[i].getType() == PrivateChannel.class) objects[i] = message == null ? null : message.getPrivateChannel();
            else if(parameters[i].getType() == Guild.class) objects[i] = message == null ? null : message.getGuild();
            else if(parameters[i].getType() == String.class) objects[i] = command;
            else if(parameters[i].getType() == Message.class) objects[i] = message;
            else if(parameters[i].getType() == JDA.class) objects[i] = this.jda;
            else if(parameters[i].getType() == MessageChannel.class) objects[i] = message == null ? null : message.getChannel();
        }
        simpleCommand.getMethod().invoke(simpleCommand.getObject(), objects);
    }
}