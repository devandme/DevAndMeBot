/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.commands;

import com.devandme.devandmebot.commands.Command.ExecutorType;
import java.lang.reflect.Method;

public final class SimpleCommand {
	
	private final String name, description;
	private final ExecutorType type;
	private final Object object;
	private final Method method;
	private final int power;
	
	public SimpleCommand(String name, String description, int power, ExecutorType type, Object object, Method method) {
		super();
		this.name = name;
		this.description = description;
		this.power = power;
		this.type = type;
		this.object = object;
		this.method = method;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}
	
	public int getPower(){
		return power;
	}

	public ExecutorType getType() {
		return type;
	}

	public Object getObject() {
		return object;
	}

	public Method getMethod() {
		return method;
	}
	
}
