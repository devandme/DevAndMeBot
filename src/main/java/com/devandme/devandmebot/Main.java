/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot;

import org.slf4j.LoggerFactory;

import com.devandme.devandmebot.core.DevAndMe;

public final class Main {
	
	/*
	 * Pour les constantes de configurations poussé, voir "utils/Constantes.java".
	 * Pour les messages, voir "utils/MessageManager.java".
	 */
	
	/*
	 * Note pour moi même : Ne pas stoquer des User mais des Long (IdLong) car s'ils changement de pseudo, ils peuvent changer de "User"
	 * Member -> Propre à chaque guild
	 * User -> Global
	 */
	
    public static void main(String[] args) throws Throwable {
        try{
            new DevAndMe().loop();
        }catch (Throwable t){
            LoggerFactory.getLogger(DevAndMe.class).error("Impossible de démarrer le bot: ", t);
            throw t;
        }
    }
}
