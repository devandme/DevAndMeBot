/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.utils;

import java.util.Comparator;
import java.util.Random;

/**
 * Comparateur aléatoire !
 *
 * @param <T> Le type d'objet à comparer.
 */
public class RandomComparator<T> implements Comparator<T> {

    private final Random random = new Random();

    @Override
    public int compare(T o1, T o2) {
        int n = random.nextInt(100);

        return n < 33 ? -1 : (n > 66 ? 1 : 0);
    }
}