/*******************************************************************************
 * This file is part of DevAndMeBot.
 *
 *     DevAndMeBot is licensed under the Apache License 2.0. You should have
 *     received a copy of the Apache License 2.0 (file LICENSE) with Dev&MeBot.
 *     If not, see <https://www.apache.org/licenses/LICENSE-2.0>.
 *******************************************************************************/
package com.devandme.devandmebot.utils;

import com.devandme.devandmebot.core.DevAndMe;
import com.devandme.devandmebot.gamel.Langage;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.entities.impl.UserImpl;

import java.util.*;
import java.util.stream.Collectors;

public abstract class Functions {

    private static final String TRUC_SWAG = "─●";

    public static String formatInteger(int i) {
        if (i / 1000000 >= 1) {
            return String.format("%.3f", (double) i / 1000000) + "M";
        }
        if (i / 1000 >= 1) {
            return String.format("%.1f", (double) i / 1000) + "k";
        }
        return Integer.toString(i);
    }

    /*
     * On essaye de lui envoyer un message. On return "true" si il est envoyer avec succès, "false" sinon.
     */
    public static boolean sendPrivateMessage(User user, String str) {
        boolean success = true;
        //On essaye d'ouvre le salon privé
        if (!user.hasPrivateChannel()) {
            try {
                user.openPrivateChannel().queue();
            } catch (Exception e) {
            	DevAndMe.getLogger().error(e.getMessage());
                success = false;
            }
        }
        //On essaye lui envoye le message
        try {
            ((UserImpl) user).getPrivateChannel().sendMessage(str).queue();
        } catch (Exception e) {
        	DevAndMe.getLogger().error(e.getMessage());
            success = false;
        }
        return success;
    }

    /**
     * Affiche les éléments de la liste.
     *
     * @param l La liste.
     * @return Les éléments séparés par des virgules.
     */
    public static String displayList(List<?> l) {
        return String.join(", ", l.stream().map(Object::toString).collect(Collectors.toList()));
    }

    /**
     * Affiche les langages de la liste.
     *
     * @param l Les langages.
     * @return Les éléments séparés par des virgules.
     */
    public static String displayLangageList(Langage[] l) {
        return String.join(", ", Arrays.stream(l).map(Langage::getName).collect(Collectors.toList()));
    }

    /**
     * Compte les rôles (ne compte pas les catégories de roles / séparators)
     *
     * @param roles        Les rôles.
     * @return Le compte.
     */
    public static long countRoles(Collection<Role> roles) {
        return roles.stream().filter(r -> !r.getName().startsWith(TRUC_SWAG)).count();
    }

    /**
     * Affiche tous les rôles (n'affiche pas les catégories de roles / séparators)
     *
     * @param roles Les rôles.
     * @param arborescence Si ya le truc swagg dedans ? WTF!?
     * @return Les rôles affichables.
     */
    public static String displayRoles(Collection<Role> roles) {
        return String.join(
                ", ",
                roles.stream()
                        .map(Role::getName)
                        .filter(r -> !r.startsWith(TRUC_SWAG))
                        .collect(Collectors.toList())
        );
    }
}
