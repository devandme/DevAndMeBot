# Participant notable au projet

Participant notable:
    - Mathéo (DiscowZombie) [Développeur principal] - 65+ commits: Base du projet, mise à jour, ajout de fonctionnalités,
    - Arthur (Arthur340/EclipseOnFire) - 7+ commits: Gestion des threads, amélioration du code vers un Java plus à jour, aide et fixs divers,
    - Théo (OriginalPainZ) - 2+ commits: Optimisation et amélioration du code,
    - RetrO L (Retr0/RetrO L) - 2+ commits: Réorganisation des package.